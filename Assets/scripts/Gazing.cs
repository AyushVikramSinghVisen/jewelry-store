﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Gazing : MonoBehaviour {
    private Camera cam;
    private RaycastHit touched;
    public GameObject ring;
    public GameObject cart;
    bool b = false;
    float i = 0.01f;
    List<string> cartItems = new List<string>();
    int items = 0;

    // Use this for initialization
    void Start () {
        cam = GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Physics.Raycast(new Ray(cam.transform.position,cam.transform.forward),out touched, 100f))
        {
            if(touched.collider.gameObject.tag == "Interactable")
            {
                checkButton(touched.collider.gameObject);
            }
            else
            {
                ring.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                i = 0.01f;
                if(b)
                {
                    Invoke("addToCart", 3.0f);
                    b = false;
                }
            }
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    void checkButton(GameObject responseObject)
    {
        if (responseObject.name == "Dismiss")
            responseObject.transform.parent.gameObject.SetActive(false);
        else if(responseObject.name == "ring")
        {
            i += 0.01f * Time.deltaTime; 
            if(i <= 0.017f)
            responseObject.transform.localScale += new Vector3(0.01f * Time.deltaTime, 0.01f * Time.deltaTime, 0.01f * Time.deltaTime);
        }
        else if(responseObject.name == "Rotate Up")
        {
            ring.transform.Rotate(Vector3.right);
        }
        else if (responseObject.name == "Rotate Down")
        {
            ring.transform.Rotate(Vector3.left);
        }
        else if (responseObject.name == "Rotate Right")
        {
            ring.transform.Rotate(Vector3.down);
        }
        else if (responseObject.name == "Rotate Left")
        {
            ring.transform.Rotate(Vector3.up);
        }
        else if (responseObject.name == "Add to Cart")
        {
            cart.SetActive(true);
            b = true;
        }
    }
    void addToCart()
    {
        cart.SetActive(false);
        cartItems.Add("Ring");
        Debug.Log("Items in Cart");
        foreach (string x in cartItems)
            Debug.Log(x);
    }
}
