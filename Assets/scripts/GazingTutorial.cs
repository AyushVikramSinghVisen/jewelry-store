﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class GazingTutorial : MonoBehaviour
{
    private Camera cam;
    private RaycastHit touched;
    public GameObject box;
    public GameObject canvas;
    public GameObject nextLevel;
    private bool check = false;

    float i = 1.5f;

    // Use this for initialization
    void Start()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Physics.Raycast(new Ray(cam.transform.position, cam.transform.forward), out touched, 100f))
        {
            if (touched.collider.gameObject.tag == "Interactable")
            {
                checkButton(touched.collider.gameObject);
            }
            else
            {
                box.transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
                i = 1.5f;
            }
        }
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    void checkButton(GameObject responseObject)
    {
        if (responseObject.name == "Dismiss")
        {
            responseObject.transform.parent.gameObject.SetActive(false);
            canvas.SetActive(true);
        }
        else if (responseObject.name == "Close")
        {
            canvas.SetActive(false);
            check = true;
        }
        else if (responseObject.name == "Cube")
        {
            i += 1.5f * Time.deltaTime;
            if (i <= 2.5f)
                responseObject.transform.localScale += new Vector3(1.5f * Time.deltaTime, 1.5f * Time.deltaTime, 1.5f * Time.deltaTime);
        }
        else if (responseObject.name == "Rotate Up")
        {
            box.transform.Rotate(Vector3.right);
            nextLevel.SetActive(check);
        }
        else if (responseObject.name == "Rotate Down")
        {
            box.transform.Rotate(Vector3.left);
            nextLevel.SetActive(check);
        }
        else if (responseObject.name == "Rotate Right")
        {
            box.transform.Rotate(Vector3.down);
            nextLevel.SetActive(check);
        }
        else if (responseObject.name == "Rotate Left")
        {
            box.transform.Rotate(Vector3.up);
            nextLevel.SetActive(check);
        }
        else if (responseObject.name == "Shop Level")
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("JewelryStore");
        }
    }
}
